# logback-rabbitmq-appender #

### What is this repository for? ###

* Custom Logback appender that writes logs to a RabbitMQ server.

### Running tests ###

* There are two test classes:
    * RabbitmqAppenderTest - simple unit test; does not write to RabbitMQ.
    * RabbitmqAppenderIntegrationTest - uses the appender as a client would slf4j, with logback configuration; NOTE: this writes to RabbitMQ so RabbitMQ must be running.

### RabbitMQ ###
RabbitMQ is a message broker - it accepts messages from producers and delivers them to consumers. The messages can be queued up in memory, or persisted to survive server crash/restart.

* I want to install the broker - https://www.rabbitmq.com/download.html
* I need a simple tutorial on using the broker - https://www.rabbitmq.com/tutorials/tutorial-one-java.html
* I need a management plugin for viewing rabbitmq features (connections, channels, queues, etc) - https://www.rabbitmq.com/management.html
* I need a command line tool for managing the broker - http://www.rabbitmq.com/man/rabbitmqctl.1.man.html

#### Useful RabbitMQ commands (assuming a linux environment) ####
* I want to see broker status information - `sudo rabbitmqctl status`
* I want to create a virtual host - `sudo add_vhost <vhostpath>`
* I want to add a user - `sudo rabbitmqctl add_user <username> <password>`
* I want to set user permissions on the virtual host - `sudo rabbitmqctl set_permissions -p <vhostpath> <username> ".*" ".*" ".*"`
    * this grants the user identified by <username> access to the virtual host with configure permission, write permission and read permission, respectively, on all resources
* I want to clear a user's permissions - `sudo rabbitmqctl clear_permissions -p <vhostpath> <username>`
* I want to check permissions on a virtual host - `sudo rabbitmqctl list_permissions -p /myvhost`
* I want to list a specific user's permissions - `sudo rabbitmqctl list_user_permissions <username>`
* I want to check my queues - `sudo rabbitmqctl list_queues -p <vhostpath> name messages_ready messages_unacknowledged`
* I want to check connections - `sudo rabbitmqctl list_connections name vhost user`
* I want to list consumers - `sudo rabbitmqctl list_consumers -p <vhostpath>`

    
### Using the appender ###
* Make sure slf4j-api, logback-core and logback-classic are in your classpath.

* Create a logback.xml that looks similar to the following:

        <?xml version="1.0" encoding="UTF-8"?>
        <configuration debug="true">
            <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
                <encoder>
                    <pattern>%d{yyyy-MM-dd_HH:mm:ss.SSS} %-5level %logger{36} - %msg%n</pattern>
                </encoder>
            </appender>

            <appender name="RABBITMQ" class="uk.co.crunch.logback.appenders.RabbitmqAppender">
                <producer>
                    <queue>audit_logs</queue>
                    <host>localhost</host>
                    <port>5672</port>
                    <user>crunch</user>
                    <password>crunch</password>
                    <virtualHost>/crunch</virtualHost>
                </producer>
            </appender>

            <logger name="AUDIT_LOGGER" level="INFO" additivity="false">
                <appender-ref ref="RABBITMQ" />
            </logger>

            <root level="INFO">
                <appender-ref ref="STDOUT" />
            </root>
        </configuration>
	
* Obtain a logger instance from your code like so:

        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;

        private static final Logger LOG = LoggerFactory.getLogger("AUDIT_LOGGER");