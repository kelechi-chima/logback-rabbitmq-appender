package uk.co.crunch.audit.appenders;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import com.rabbitmq.client.*;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.helpers.LogLog;
import org.apache.log4j.spi.LoggingEvent;

import uk.co.crunch.audit.appenders.com.squareup.tape.QueueFile;

import com.google.gson.Gson;

/**
 * Log4J appender that writes logs to a RabbitMQ server.
 */
public class RabbitmqAppender extends AppenderSkeleton {

    private final AtomicBoolean initializing = new AtomicBoolean();

    private final Gson gson = new Gson();

    private Connection connection;
    private Channel channel;

    private ExecutorService senderPool = null;

    //private int senderPoolSize = 2;

    private QueueFile queueFile;

    private String queue;
    private String host;
    private int port;
    private String user;
    private String password;
    private String virtualHost;
    private String queueFilename;

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public String getQueueFilename() {
        return queueFilename;
    }

    public void setQueueFilename(String queueFilename) {
        this.queueFilename = queueFilename;
    }

    @Override
    public void close() {
        if (senderPool != null) {
            senderPool.shutdown();

            try {
                if (!senderPool.awaitTermination(30, TimeUnit.SECONDS)) {
                    senderPool.shutdownNow();
                    if (!senderPool.awaitTermination(30, TimeUnit.SECONDS)) {
                        LogLog.error("Log sender pool did not terminate");
                    }
                }
            } catch (InterruptedException e) {
                senderPool.shutdownNow();
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public boolean requiresLayout() {
        return false;
    }

    @Override
    protected void append(LoggingEvent event) {
        if (senderPool == null && initializing.compareAndSet(false, true)) {
            try {
                queueFile = new QueueFile(new File(queueFilename));

                ConnectionFactory connectionFactory = new ConnectionFactory();
                connectionFactory.setHost(host);
                connectionFactory.setPort(port);
                connectionFactory.setUsername(user);
                connectionFactory.setPassword(password);
                connectionFactory.setVirtualHost(virtualHost);
                connection = connectionFactory.newConnection();
                channel = connection.createChannel();
                channel.queueDeclare(queue, true, false, false, null);
                channel.confirmSelect();

                LogSender logSender = new LogSender();
                channel.addConfirmListener(logSender);

                senderPool = Executors.newCachedThreadPool();
                senderPool.submit(logSender);
            } catch (IOException e) {
                LogLog.error("RabbitmqAppender was not initialised properly", e);
            } finally {
                initializing.set(false);
            }
        }
    }

    protected class LogSender implements Runnable, ConfirmListener {

        @Override
        public void run() {
            while (true) {
                try {
                    readAndSend();
                } catch (IOException e) {
                    LogLog.error("Error processing log in producer thread, not removed from queue file.", e);
                } catch (InterruptedException e) {
                    LogLog.error("Producer thread interrupted, will attempt to flush remaining logs.");
                    Thread.currentThread().interrupt();
                    break;
                }
            }

            while (!queueFile.isEmpty()) {
                try {
                    readAndSend();
                } catch (IOException e) {
                    LogLog.error("Error processing log in producer thread, not removed from queue file.", e);
                } catch (InterruptedException e) {
                    LogLog.error("Producer interrupted while flushing remaining logs, exiting");
                    Thread.currentThread().interrupt();
                    break;
                }
            }

            cleanUp();
        }
        
        @Override
        public void handleAck(long deliveryTag, boolean multiple) throws IOException {
            queueFile.remove();
        }

        @Override
        public void handleNack(long deliveryTag, boolean multiple) throws IOException {
            // message delivery failed, do not remove from queue file
        }

        private void readAndSend() throws IOException, InterruptedException {
            byte[] bytes = queueFile.peek();

            if (bytes != null && bytes.length > 0) {
                channel.basicPublish("", queue, MessageProperties.PERSISTENT_TEXT_PLAIN, bytes);
                channel.waitForConfirms();
            }
        }

        private void cleanUp() {
            try {
                channel.close();
            } catch (IOException | ShutdownSignalException e) {
            }

            try {
                connection.close();
            } catch (IOException | ShutdownSignalException e) {
            }
        }
    }
}
