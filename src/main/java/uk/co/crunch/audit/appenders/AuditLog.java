package uk.co.crunch.audit.appenders;

public class AuditLog {

    private final String timestamp;
    private final String action;
    private final Long personId;
    private final String actionStatus;
    private final String actionOutcome;

    public AuditLog(String timestamp, String action, Long personId, String actionStatus, String actionOutcome) {
        this.timestamp = timestamp;
        this.action = action;
        this.personId = personId;
        this.actionStatus = actionStatus;
        this.actionOutcome = actionOutcome;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getAction() {
        return action;
    }

    public Long getPersonId() {
        return personId;
    }

    public String getActionStatus() {
        return actionStatus;
    }

    public String getActionOutcome() {
        return actionOutcome;
    }

    @Override
    public String toString() {
        return "AuditLog {" +
                "timestamp=" + timestamp +
                ", action=" + action +
                ", personId=" + personId +
                ", actionStatus=" + actionStatus +
                ", actionOutcome=" + actionOutcome +
                "}";
    }
}
